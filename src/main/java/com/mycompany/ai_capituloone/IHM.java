package com.mycompany.ai_capituloone;

import java.util.ArrayList;

public interface IHM {
    int PedirValorEntero(String pregunta);
    boolean PedirValorBooleano(String pregunta);
    void MostrarHechos(ArrayList<IHecho> hechos);
    void MostrarReglas(ArrayList<Regla> reglas);
}

/**
 * Dos métodos; solicitan información al usuario. Acerca de un hecho entero o booleano.
 * Dos métodos; mostrar los hechos y las reglas.
 */