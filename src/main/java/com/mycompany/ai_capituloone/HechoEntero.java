/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ai_capituloone;

/**
 *
 * @author jose
 */
public class HechoEntero implements IHecho{
    protected String nombre;
    @Override
    public String Nombre() {
        return nombre;
    }
    
    protected int valor;
    @Override
    public Object Valor() {
        return valor;
    }
    
    protected int nivel;
    @Override
    public int Nivel() {
        return nivel;
    }
    
    protected String pregunta = "";
    @Override
    public String Pregunta() {
        return pregunta;
    }

    @Override
    public void setNivel(int l) {
        nivel = l;
    }
    
    //-------------------------------------------------------------------------- CONSTRUCTOR
    public HechoEntero(String _nombre,int _valor,String _pregunta,int _nivel){
        nombre = _nombre;
        valor = _valor;
        pregunta = _pregunta;
        nivel = _nivel;
    }
    
    //-------------------------------------------------------------------------- MÉTODO toString
    @Override
    public String toString() {
        return nombre + "=" + valor +" ("+ nivel + ")";
    }
    
}
