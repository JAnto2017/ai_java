/**
 * Contiene reglas que inicializa la lista.
 * Cuando se crea una nueva base a partir de una lista de reglas. Hay que copiarlas una a una.
 * Evitar que la eliminaciń de una regla, provoque la eliminación del contenido de la base.
 * 
 */

package com.mycompany.ai_capituloone;

import java.util.ArrayList;


public class BaseDeReglas {
    protected ArrayList<Regla> reglas;

    public ArrayList<Regla> getReglas() {
        return reglas;
    }
    public void setReglas(ArrayList<Regla> _reglas) {
        //se copian y agregan reglas
        for (Regla r : _reglas) {
            Regla copia = new Regla(r.nombre, r.premisas, r.conclusion);
            reglas.add(copia);
        }
    }

    public BaseDeReglas() {
        reglas = new ArrayList<>();
    }

    //métodos para agregar, eliminar o vaciar toda la base de reglas
    public void ClearBase() {
        reglas.clear();
    }
    public void AgregarRegla(Regla re) {
        reglas.add(re);
    }
    public void Eliminar(Regla r) {
        reglas.remove(r);
    }
}
