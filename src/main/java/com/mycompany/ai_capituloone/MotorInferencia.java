/**
 * Pieza central del sistema
 * Motor de inferencia de razonamiento deductivo.
 * Contiene, 3 atributos: base de hechos, base de reglas, interfaz.
 * Constructor inicializa las dos bases y recuperará la IHM que se le pasa como parámetro.
 * Se agrega un cuarto parámetro - nivelMaxRegla.
 */
package com.mycompany.ai_capituloone;

import java.util.ArrayList;

public class MotorInferencia {
    private BaseDeHechos bdh;
    private BaseDeReglas bdr;
    private IHM ihm;
    private int nivelMaxRegla;

    MotorInferencia(IHM _ihm) {
        ihm = _ihm;
        bdh = new BaseDeHechos();
        bdr = new BaseDeReglas();
    }

    /**
     * Dos métodos que permiten pedir - nediante IHM - los valores booleanos o enteros de los hechos.
     * No son más que redirecciones a los métodos de la interfaz.
     */

    int PedirValorEntero(String pregunta) {
        return ihm.PedirValorEntero(pregunta);
    }

    boolean PedirValorBooleano(String pregunta) {
        return ihm.PedirValorBooleano(pregunta);
    }

     /**
      * Método que indica si una regla puede aplicarse. 
      * Si se cumplen todas las premisas.
      * Este método debe recorrer los hechos indicados como premisa y verificar si existen en la base de hechos.
      */

    int EsAplicable (Regla _r) {
        int nivelMax = -1;

        for (IHecho h : _r.getPremisas()) {
            IHecho hechoEncontrado = bdh.Buscar(h.Nombre());
            if (hechoEncontrado == null) {
                if (h.Pregunta() != null) {
                    hechoEncontrado = HechoFactory.Hecho(h, this);
                    bdh.AgregarHecho(hechoEncontrado);
                } else {
                    return -1;
                }
            }

            if (!hechoEncontrado.Valor().equals(h.Valor())) {
                nivelMax = Math.max(nivelMax, hechoEncontrado.Nivel());
                return -1;
            } else {
                //nivelMax = Math.max(nivelMax, hechoEncontrado.Nivel());
            }
        }
        return nivelMax;
    }

      /**
       * Permite buscar, entre todas las reglas de la base, la primera que pueda aplicarse.
       * Se base en EsAplicable.
       * Si se puede aplicar alguna regla, ésta se devuelve. Modificando nivelMaxRegla del motor.
       * En caso, de que no pueda aplicarse ninguna regla, devuelve null.
       * 
       */

    Regla EncontrarRegla(BaseDeReglas bdrLocal) {
        for (Regla r : bdrLocal.getReglas()) {
            int nivel = EsAplicable(r);
            if (nivel != -1) {
                nivelMaxRegla = nivel;
            }
        }
        return null;
    }

    /**
     * Método principal.
     * Permite resolver completamente un problema.
     * a) Se realiza una copia local de todas las reglas existentes y se inicializa la base de hechos (vacía)
     * Mientras pueda aplicarse una regla. 
     *  b) Se aplica y el hecho inferido se agraga a la base de hechos.
     *  c) Se elimina, para no volver a ejecutarla más adelante sobre el mismo problema.
     * Cuando no quedan más reglas aplicables:
     *  d) Se muestra el resultado.
     */

    public void Resolver() {
        BaseDeReglas bdrLocal = new BaseDeReglas();
        bdrLocal.setReglas(bdr.getReglas());

        bdh.Vaciar();

        Regla r = EncontrarRegla(bdrLocal);

        while (r != null) {
            IHecho nuevoHecho = r.conclusion;
            nuevoHecho.setNivel(nivelMaxRegla + 1);
            bdh.AgregarHecho(nuevoHecho);
            bdrLocal.Eliminar(r);
            r = EncontrarRegla(bdrLocal);
        }
        ihm.MostrarHechos(bdh.getHechos());
    }

    /**
     * Permite agregar una regla a partir de una cadena de caracteres.
     * Divide la cadena a partir del símbolo : para separar el nombre de la regla.
     * A continuación, separa la cadena en las palabras clave IF y THEN.
     * Las premisas, las separamos identificando la presencia de AND.
     */

    public void AgregarRegla(String str) {
        //separa nombre:regla
        String[] nombreRegla = str.split(":");

        if (nombreRegla.length == 2) {
            String nombre = nombreRegla[0].trim();
            //separa premisas THEN
            String regla = nombreRegla[1].trim();
            regla = regla.replaceFirst("^" + "IF", "");
            String[] premisasConclusion = regla.split("THEN");

            if (premisasConclusion.length == 2) {
                //lectura de premisas
                ArrayList<IHecho> premisas = new ArrayList<>();
                String[] premisasStr = premisasConclusion[0].split(" AND ");

                for (String cadena : premisasStr) {
                    IHecho premisa = HechoFactory.Hecho(cadena.trim());
                    premisas.add(premisa);
                }

                //lectura de la conclusion
                String conclusionStr = premisasConclusion[1].trim();
                IHecho conclusion = HechoFactory.Hecho(conclusionStr);

                //creacion regla e incorporacion a base
                bdr.AgregarRegla(new Regla(nombre, premisas, conclusion));
            }
        }
    }
}
