/**
 * Esta clase se encarga de crear un hecho del tipo adecuado y devolverá un IHecho.
 * 
 * Permite preguntar al usuario el valor de un hecho y crearlo.
 * Para agregarlo, a la base de hechos.
 * El hecho puede ser de distintos tipos -int, boolean- de cara a ser lo más genérico posible se utiliza una clase estática como fábrica de hechos.
 */

package com.mycompany.ai_capituloone;

public class HechoFactory {
    static IHecho Hecho(IHecho f, MotorInferencia m) {
        try {
            IHecho nuevoHecho;
            Class clase = f.getClass();
            if (clase.equals(Class.forName("com.mycompany.ai_capituloone.HechoEntero"))) {
                nuevoHecho = CrearHechoEntero(f,m);
            } else {
                nuevoHecho = CrearHechoBooleano(f,m);
            }
            return nuevoHecho;
        } catch (Exception e) {
            System.err.println("Error clase HechoFactory método static Hecho");
            return null;
        }
    }
    //------------------------------------------------------------------------------------------------------
    static IHecho CrearHechoEntero(IHecho f, MotorInferencia m){
        int valor = m.PedirValorEntero(f.Pregunta());
        return new HechoEntero(f.Nombre(), valor, null, 0);
    }
    //------------------------------------------------------------------------------------------------------
    static IHecho CrearHechoBooleano(IHecho f, MotorInferencia m){
        boolean valorB = m.PedirValorBooleano(f.Pregunta());
        return new HechoBooleano(f.Nombre(), valorB, null, 0);
    }

    /**
     * Permite crear un hecho a partir de una cadena.
     * Los hechos se expresarán con la forma:
     *  "Nombre=Valor(pregunta)" para un hecho entero
     *  "Nombre(pregunta)"/"!Nombre(pregunta)" para un hecho booleano
     */

    static IHecho Hecho(String hechoStr) {
        hechoStr = hechoStr.trim();

        if (hechoStr.contains("=")) {
            //existe el símbolo =, se trata de un hecho entero
            hechoStr = hechoStr.replaceFirst("^" + "\\(", "");
            String[] nombreValorPregunta = hechoStr.split("[=()]");
            
            if (nombreValorPregunta.length >= 2) {
                String pregunta = null;
                if (nombreValorPregunta.length == 3) {
                    pregunta = nombreValorPregunta[2].trim();
                }
                return new HechoEntero(nombreValorPregunta[0].trim(), Integer.parseInt(nombreValorPregunta[1].trim()), pregunta, 0);
            }
        } else {
            //es un hecho booleano
            boolean valor = true;
            if (hechoStr.startsWith("!")) {
                valor = false;
                hechoStr = hechoStr.substring(1).trim();
            }
            //Split tras eliminar el primer delimitador si es necesario : "("
            hechoStr = hechoStr.replaceFirst("^" + "\\(", "");
            String[] nombrePregunta = hechoStr.split("[()]");
            String pregunta = null;
            if (nombrePregunta.length == 2) {
                pregunta = nombrePregunta[1].trim();
            }
            return new HechoBooleano(nombrePregunta[0].trim(), valor, pregunta, 0);
        }
        return null;
    }
}
