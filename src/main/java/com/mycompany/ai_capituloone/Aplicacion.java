package com.mycompany.ai_capituloone;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Aplicacion implements IHM{
    
    public static void main(String[] args) {
        Aplicacion app = new Aplicacion();
        app.Run();
    }
    //-----------------------------------------------------------------------
    /**
     * crea un nuevo motor.
     * agrega las reglas - versión texto -
     * 11 reglas a los triángulos y cuadriláteros.
     */
    public void Run() {
        System.out.println("** CREACIÓN DEL MOTOR **");
        MotorInferencia m = new MotorInferencia(this);
        //agrega las reglas
        System.out.println("** AGREGADAS LAS REGLAS **");
        m.AgregarRegla("R1 : IF (Orden=3 (¿Cuál es el orden?)) THEN Triángulo");
        m.AgregarRegla("R2 : IF (Triángulo AND Ángulo Recto (¿La figura tiene al menos un ángulo recto)) THEN Triángulo Rectángulo");
        m.AgregarRegla("R3 : IF (Triángulo AND Lados Iguales = 2 (¿Cuántos lados iguales tiene la figura?)) THEN Triángulo Isósceles");
        m.AgregarRegla("R4 : IF (Triángulo rectángulo AND Triángulo Isósceles) THEN Triángulo Rectángulo Isósceles");
        m.AgregarRegla("R5 : IF (Triángulo AND Lados Iguales = 3 (¿Cuántos lados iguales tiene la figura?)) THEN Triángulo Equilátero");
        m.AgregarRegla("R6 : IF (Orden =  4 (¿Cuál es el orden?)) THEN Cuadrilátero");
        m.AgregarRegla("R7 : IF (Cuadrilátero AND Lados Paralelos = 2 (¿Cuántos lados paralelos entre sí - 0, 2 ó 4 ?)) THEN Trapecio");
        m.AgregarRegla("R8 : IF (Cuadrilátero AND Lados Paralelos = 4 (¿Cuántos lados paralelos entre sí - 0, 2 ó 4 ?)) THEN Paralelogramo");
        m.AgregarRegla("R9 : IF (Paralelogramo AND Ángulo Recto (¿La figura tiene al menos un ángulo recto?)) THEN Rectángulo");
        m.AgregarRegla("R10 : IF (Paralelogramo AND Lados Iguales = 4 (¿Cuántos lados iguales tiene la figura?)) THEN Rombo");
        m.AgregarRegla("R11 : IF (Rectángulo AND Rombo THEN Cuadrado");
        //resolución
        while (true) {
            System.out.println("\n** RESOLUCIÓN **");
            m.Resolver();
        }
    }
    //-----------------------------------------------------------------------
    @Override
    public int PedirValorEntero(String pregunta) {
        System.out.println(pregunta);
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            return Integer.decode(br.readLine());
        } catch (Exception e) {
            System.err.println("Error Class Aplicacion - método PedirValorEntero");
            return 0;
        }
        
    }
    //-----------------------------------------------------------------------
    /**
     * Solicita valor booleano, con si ó no (si => verdadero)
     * Los errores se ignoran (devuelve falso)
     */
    @Override
    public boolean PedirValorBooleano(String pregunta) {
        try {
            System.out.println(pregunta + " (si, no)");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String res = br.readLine();
            return (res.equals("si"));
        } catch (Exception e) {
            System.err.println("Error Class Aplicacion - método PedirValorBooleano");
            return false;
        }
        
    }
    //-----------------------------------------------------------------------
    /**
     * Para la visualización de los hechos, se deben ordenar decreciente de nivel (método sort)
     * Se quitan los de nivel 0 (hecho intro por usuario)
     * De este modo: los hechos obtenidos al final y de más alto nivel se mostrarán primero.
     * "Triángulo rectángulo" se mostrará antes que "Triángulo"
     */
    @Override
    public void MostrarHechos(ArrayList<IHecho> hechos) {
        String res = "Solucion(es) encontrada(s) : \n";

        Collections.sort(hechos, (IHecho f1, IHecho f2) -> {
            return Integer.compare(f1.Nivel(), f2.Nivel());
        });
        for (IHecho h : hechos) {
            if (h.Nivel() != 0) {
                res += h.toString() + "\n";
            }
        }
        System.out.println(res);        
    }
    //-----------------------------------------------------------------------
    @Override
    public void MostrarReglas(ArrayList<Regla> reglas) {
        String res = "";
        for (Regla r : reglas) {
            res += r.toString() + "\n";
        }        
        System.out.println(res);
    }
}
