/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ai_capituloone;

import java.util.ArrayList;

/**
 *
 * @author jose
 */
public class BaseDeHechos {
    
    protected ArrayList<IHecho> hechos;
    
    public ArrayList<IHecho> getHechos() {
        return hechos;
    }
    
    //-------------------------------------------------------------------------- CONSTRUCTOR
    public BaseDeHechos() {
        hechos = new ArrayList<IHecho>();
    }
    
    //-------------------------------------------------------------------------- MÉTODO vaciar elemento de la base
    public void Vaciar() {
        hechos.clear();
    }
    
    //-------------------------------------------------------------------------- MÉTODO agrgar elemento a la base
    public void AgregarHecho(IHecho hecho) {
        hechos.add(hecho);
    }
    
    //-------------------------------------------------------------------------- MÉTODO busca un hecho de la base
    /**
     * 
     * @param nombre
     * @return el hecho si lo encuentra o null en caso contrario
     */
    public IHecho Buscar(String nombre) {
        for (IHecho hecho : hechos) {
            if (hecho.Nombre().equals(nombre)) {
                return hecho;
            }
        }
        return null;
    }
    
    //-------------------------------------------------------------------------- MÉTODO recuperar valor de un hecho
    /**
     * 
     * @param nombre que sirve para encontrar el hecho
     * @return valor de un hecho, si no existe devuelve null
     */
    public Object RecuperarValorHecho(String nombre) {
        for (IHecho hecho : hechos) {
            if (hecho.Nombre().equals(nombre)) {
                return hecho.Valor();
            }
        }
        return null;
    }
}
