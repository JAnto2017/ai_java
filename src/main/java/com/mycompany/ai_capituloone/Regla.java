/**
 * Las Reglas contienen tres atributos con los setters y getters correspondientes:
 * 1.- un nombre
 * 2.- una lista de hechos que forman las premisas de la regla
 * 3.- un hecho, que es la conclusión de la regla
 */
package com.mycompany.ai_capituloone;

import java.util.ArrayList;
import java.util.StringJoiner;

public class Regla {
    
    protected ArrayList<IHecho> premisas;
    public ArrayList<IHecho> getPremisas() {
        return premisas;
    }
    public void setPremisas(ArrayList<IHecho> premisas) {
        this.premisas = premisas;
    }

    protected IHecho conclusion;
    public IHecho getConclusion() {
        return conclusion;
    }
    public void setConclusion(IHecho conclusion) {
        this.conclusion = conclusion;
    }

    protected String nombre;
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Regla(String _nombre,ArrayList<IHecho> _premisas,IHecho _conclusion) {
        nombre = _nombre;
        premisas = _premisas;
        conclusion = _conclusion;
    }

    @Override
    public String toString() {
        String cadena = nombre + " : IF (";

        //StringJoiner permite concatenar cadenas con un separador - en nuestro caso es AND -
        StringJoiner sj = new StringJoiner(" AND ");
        for (IHecho hecho : premisas) {
            sj.add(hecho.toString());
        }

        cadena += sj.toString() + ") THEN " + conclusion.toString();
        return cadena;
    }
}
