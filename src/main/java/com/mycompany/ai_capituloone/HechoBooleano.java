/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ai_capituloone;

/**
 *
 * @author jose
 */
public class HechoBooleano implements IHecho{
    protected String nombre;
    @Override
    public String Nombre() {        
        return nombre;
    }
    
    protected boolean valor;
    @Override
    public Object Valor() {        
        return valor;
    }
    
    protected int nivel;
    @Override
    public int Nivel() {        
        return nivel;
    }

    @Override
    public void setNivel(int l) {        
        nivel = l;
    }

    protected String pregunta = null;
    @Override
    public String Pregunta() {
        
        return pregunta;
    }
    
    //-------------------------------------------------------------------------- CONSTRUCTOR
    public HechoBooleano(String _nombre,boolean _valor,String _pregunta,int _nivel){
        nombre = _nombre;
        valor = _valor;
        pregunta = _pregunta;
        nivel = _nivel;
    }
    //-------------------------------------------------------------------------- MÉTODO toString
    @Override
    public String toString(){
        String res = "";
        if (!valor) {
            res += "!";
        }
        res += nombre + " ("+ nivel + ")";
        return res;
    }
}
